package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
)

var t = template.Must(template.New("mainTemplate").Parse(mainTemplate))

func servicePingHandler(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	ns := r.FormValue("namespace")

	if name == "" || ns == "" {
		http.Error(w, "Missing form value", http.StatusBadRequest)
		return
	}

	resp, err := http.Get(fmt.Sprintf("http://%s.%s.svc.cluster.local:5000", name, ns))

	data := struct {
		Response *http.Response
		Error    error
	}{
		resp,
		err,
	}
	if err := t.Execute(w, data); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "GET":
			t.Execute(w, nil)
		case "POST":
			servicePingHandler(w, r)
		default:
			http.NotFound(w, r)
		}
	})

	port := os.Getenv("PORT")
	if port == "" {
		port = "5000"
	}
	addr := ":" + port

	log.Println("Listening on", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
